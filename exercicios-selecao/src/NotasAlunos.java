import java.util.Scanner;

public class NotasAlunos {
	
	//M�todo de calculo de m�dia do aluno
	public static float mediaNota(float n1, float n2, float opt) {
		float result = 0;
		
		//A nota da prova optativa substituir� a nota mais baixa entre as outras provas
		//Caso o aluno n�o fa�a a avalia��o, o calculo s� ser� feito entre as duas primeiras provas. (opt = -1)
		if(opt > n1) {
			result = (opt + n2)/ 2;
		}else if (opt > n2) {
			result = (opt + n1) / 2;
		}else {
			result = (n1 + n2) / 2;
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o de Vari�veis	
		String nome; 
		float a1, a2, opt = -1, media; //Duas avalia��es normais e a avalia��o optativa
		char op;
		
		System.out.println("Digite o nome do aluno: ");
		nome = teclado.nextLine();	
		
		System.out.println("Digite a primeira nota do aluno:");
		a1 = teclado.nextFloat();
		
		System.out.println("Digite a segunda nota do aluno:");
		a2 = teclado.nextFloat();
		
		System.out.println("O aluno fez a avalia��o optativa? (s / n)");
		op = teclado.next().charAt(0);
		
		switch(op) {
			case 's':
				System.out.println("Digite o valor da nota do aluno:");
				opt = teclado.nextFloat();
				
				media = mediaNota(a1, a2, opt);
				
				if(media >= 3 && media <= 6) {
					System.out.println("Primeira nota: " + a1);
					System.out.println("Segunda nota: " + a2);
					System.out.println("Nota da Prova Optativa: " + opt);
					System.out.println("M�dia do aluno: " + media);
					System.out.println("O aluno " + nome + " est� em exame!");
				}else if(media >= 6) {
					System.out.println("Primeira nota: " + a1);
					System.out.println("Segunda nota: " + a2);
					System.out.println("Nota da Prova Optativa: " + opt);
					System.out.println("M�dia do aluno: " + media);
					System.out.println("O aluno " + nome + " est� em aprovado!");
				}else{
					System.out.println("Primeira nota: " + a1);
					System.out.println("Segunda nota: " + a2);
					System.out.println("Nota da Prova Optativa: " + opt);
					System.out.println("M�dia do aluno: " + media);
					System.out.println("O aluno " + nome + " est� em reprovado!");
				}
			
			case 'n':
				media = mediaNota(a1, a2, opt);
				
				if(media >= 3 && media <= 6) {
					System.out.println("Primeira nota: " + a1);
					System.out.println("Segunda nota: " + a2);
					System.out.println("Nota da Prova Optativa: " + opt);
					System.out.println("M�dia do aluno: " + media);
					System.out.println("O aluno " + nome + " est� em exame!");
				}else if(media >= 6) {
					System.out.println("Primeira nota: " + a1);
					System.out.println("Segunda nota: " + a2);
					System.out.println("Nota da Prova Optativa: " + opt);
					System.out.println("M�dia do aluno: " + media);
					System.out.println("O aluno " + nome + " est� em aprovado!");
				}else{
					System.out.println("Primeira nota: " + a1);
					System.out.println("Segunda nota: " + a2);
					System.out.println("Nota da Prova Optativa: " + opt);
					System.out.println("M�dia do aluno: " + media);
					System.out.println("O aluno " + nome + " est� em reprovado!");
				}
			default:
					System.out.println("Op��o Inv�lida!");
			}
		}
			
		
	}