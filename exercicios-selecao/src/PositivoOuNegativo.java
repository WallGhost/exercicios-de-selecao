import java.util.Scanner;

public class PositivoOuNegativo {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);		
		int numero;
		
		System.out.println("Digite um n�mero inteiro:");
		numero = teclado.nextInt();
		
		if(numero < 0) {
			System.out.println("O n�mero digitado � negativo!");
		}else if(numero == 0) {
			System.out.println("O n�mero digitado � ZERO!");
		}else {
			System.out.println("O n�mero digitado � positivo!");
		}
		
	}

}
