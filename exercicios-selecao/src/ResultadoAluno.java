import java.util.Scanner;

public class ResultadoAluno {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o de vari�veis
		float a1, a2, media;
		
		System.out.println("Digite a primeira nota do aluno: ");
		a1 = teclado.nextFloat();
		
		System.out.println("Digite a segunda nota do aluno: ");
		a2 = teclado.nextFloat();
		
		media = (a1 + a2) / 2;
		
		if(media >= 6) {
			System.out.println("M�dia do aluno: " + media);
			System.out.println("PARAB�NS! Voc� foi APROVADO!");
		}else {
			System.out.println("M�dia do aluno: " + media);
			System.out.println("Voc� foi REPROVADO! Estude mais...");
		}
		
	}

}
